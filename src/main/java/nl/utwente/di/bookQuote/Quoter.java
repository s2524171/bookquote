package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String isbn) {
            HashMap<String, Double> getBookPrice = new HashMap<String, Double>();
            getBookPrice.put("1", 10.0);
            getBookPrice.put("2", 45.0);
            getBookPrice.put("3", 20.0);
            getBookPrice.put("4", 35.0);
            getBookPrice.put("5", 50.0);

            return getBookPrice.getOrDefault(isbn, 0.0);

        }
    }

